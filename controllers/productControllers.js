const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create a new product

module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})
	return newProduct.save().then((product, error)=>{
		if(error){
			return false;
		}
		else{
			return "Succesfully added new product";
		}
	})
}

// Retrieve a specific Product ID

module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}

// Retrieve all active products
module.exports.getAllActive = () =>{
	return Product.find({isActive: true}).then(result => result);
}

// Router to update a product

module.exports.updateProduct = (productId, reqBody) =>{
	// Specify the fields/properties to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

// Router to Archive

module.exports.archiveProduct = (productId) =>{
	let updateActiveField = {
		isActive : false
	}

	return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
		// Product is not archived
		if(error){
			return false;
		}
		// Product archived successfully
		else{
			return true
		}
	})
}

// Retrieve all Archive (inActive)

module.exports.checkInActiveProducts = () =>{
	return Product.find({isActive: false}).then(result => result);
}



